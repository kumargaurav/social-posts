<?php 
namespace App\Helper;



use Auth;
use App\Models\Blog;
use App\Models\Comment;
use App\Models\Trend;
use DB;
/**
 * Class Helper
 * @package App\Helper
 */
class Helper{

	function getRole(){
		$data=Auth::user();
		return $data->type;
	}

	function getId(){
		$data=Auth::user();
		return $data->id;
	}

	function getDataBlogComment(){
		if($this->getRole()=="admin"){
			$data=Comment::orderBy("created_at","desc")->get();
		}else{
			$data=Comment::where('user_id',$this->getId())->orderBy("created_at","desc")->get();
		}
		$newData=[];
		foreach($data as $d){
			$newData1=$d->toArray();
			$newData1['username']=$d->username;
			$newData1['email']=$d->email;
			$newData[]=$newData1;
		}

		return $newData;
	}

	function getDataBlog(){
		if($this->getRole()=="admin"){
			$data=Blog::orderBy("created_at","desc")->get();
		}else{
			$data=Blog::where('user_id',$this->getId())->orderBy("created_at","desc")->get();
		}
		$newData=[];
		foreach($data as $d){
			$newData1=$d->toArray();
			$newData1['username']=$d->username;
			$newData1['email']=$d->email;
			$newData1['commentcount']=$d->commentcount;
			$newData1['comment-list']=$this->getCommentData($d->comments);
			$newData[]=$newData1;
		}

		return $newData;
	}
	function getCommentData($data){
		$newData=[];
		foreach($data as $d){
			$newData1=$d->toArray();
			$newData1['username']=$d->username;
			$newData1['email']=$d->email;
			$newData[]=$newData1;
		}

		return $newData;
	}

	function getTrendingBlogHelper(){
		$data=DB::select(DB::raw("select count(blog_id) as count,blog_id from trends WHERE created_at >= NOW() - INTERVAL 6 HOUR group by blog_id order by count desc"));
		$ids=[];
		foreach($data as $d){
			$ids[]=$d->blog_id;
		}	
		$data=Blog::whereIn("id",$ids)->orderBy("created_at","desc")->get();
		
		$newData=[];
		foreach($data as $d){
			$newData1=$d->toArray();
			$newData1['username']=$d->username;
			$newData1['email']=$d->email;
			$newData1['commentcount']=$d->commentcount;
			$newData1['comment-list']=$this->getCommentData($d->comments);
			$newData[]=$newData1;
		}

		return $newData;
	}

	function getValidActionByBlogID($blog){
		if($this->getRole()=="admin"){
			return true;
		}else{
			if($blog->user_id==$this->getId()){
				return true;
			}else{
				return false;
			}
		}
		return $data;
	}

	function getValidActionByCommentID($comment){
		if($this->getRole()=="admin"){
			return true;
		}else{
			if($comment->user_id==$this->getId()){
				return true;
			}else{
				return false;
			}
		}
		return $data;
	}


	function getLocation($lng,$lat){
		
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
	     $json = @file_get_contents($url);
	     $data=json_decode($json);

	     $status = $data->status;
	     if($status=="OK")
	     {
	       return $data->results[0]->formatted_address;
	     }
	     else
	     {
	       return false;
	     }
	}
}