<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Blog; 
use App\Models\Trend; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Helper;
use DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Helper::getDataBlog();
        return response()->json(['data' => $data,"status"=>200]);     
    }

  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $validator = Validator::make($request->all(), [ 
                'name' => 'required', 
                'description' => 'required', 
                'seo_url' => 'required|alpha_dash|unique:blogs,seo_url', 
                'title'=> 'required', 
                'location'=> 'required' ,
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors(),"status"=>401], 401);            
            }

            $data=$request->all();
            $data['longitude']="77.3812";
            $data['latitude']="28.6210";
            $data['user_id']=Helper::getId();
            $data=Blog::create($data);
            $success['name']=$data->name;
            $success['type']="Blog Successfully created";
            return response()->json(['success'=>$success,"status"=>200], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
                'blog_id' => 'required',
                
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors(),"status"=>401], 401);
            }
            $data=Blog::find($request->blog_id);
            if($data){
                Trend::create(["user_id"=>Helper::getId(),"blog_id"=>$request->blog_id]);
                $newData=$data->toArray();
                $newData['username']=$data->username;
                $newData['email']=$data->email;

                $newData['commentcount']=$data->commentcount;
                $newData['comment-list']=Helper::getCommentData($data->comments);
                
                return response()->json(['data' => $newData,"status"=>200]);
                
            }else{
                return response()->json(['error'=>'Invalid Blog id',"status"=>404], 404); 
            }
              
    }

   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
                'blog_id' => 'required',
                'name' => 'required', 
                'description' => 'required', 
                'seo_url' => 'required|alpha_dash|unique:blogs,seo_url,'.$request->blog_id, 
                'title'=> 'required'  
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors(),"status"=>401], 401);
            }
            $data=Blog::find($request->blog_id);
            if($data){
                if(Helper::getValidActionByBlogID($data)){
                    $data->update($request->all());
                    return response()->json(['success' => "Successfully Edited","status"=>200]);
                }else{
                    return response()->json(['error'=>'Unauthorised',"status"=>401], 401); 
                }
            }else{
                return response()->json(['error'=>'Invalid Blog id',"status"=>404], 404); 
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
                'blog_id' => 'required', 
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors(),"status"=>401], 401);
            }
            $data=Blog::find($request->blog_id);
            if($data){
                if(Helper::getValidActionByBlogID($data)){
                    $data->delete();
                    return response()->json(['success' => "Successfully deleted","status"=>200]);
                }else{
                    return response()->json(['error'=>'Unauthorised',"status"=>401], 401); 
                }
            }else{
                return response()->json(['error'=>'Invalid Blog id',"status"=>404], 404); 
            }
              
    }


    public function getTrendingBlog(){
        $data=Helper::getTrendingBlogHelper();
        return response()->json(['data' => $data,"status"=>200]);  
    }

    public function search(Request $request){
        $data=[];

        $data=Blog::query();
        if($request->longitude && $request->latitude && $request->radius)
        {
    
            $data->where(DB::raw( "ACOS( SIN( RADIANS(  `latitude` ) ) * SIN( RADIANS(  '".$request->latitude."' ) ) + COS( RADIANS(  `latitude` ) ) * COS( RADIANS(  '28.6210' ) ) * COS( RADIANS( `longitude` ) - RADIANS(  '".$request->longitude."' ) ) ) *6380"),'<=',$request->radius);

        }
        if($request->location){
            $data->where("location",$request->location);
        }
        if($request->name){
            $data->where("name","like","%".$request->name."%");
        }
        if($request->created_date_time){
            $data->where("created_at",$request->created_date_time);
        }
        $data=$data->get();

        $newData=[];
        foreach($data as $d){
            $newData1=$d->toArray();
            $newData1['username']=$d->username;
            $newData1['email']=$d->email;
            $newData1['commentcount']=$d->commentcount;
            $newData1['comment-list']=Helper::getCommentData($d->comments);
            $newData[]=$newData1;
        }

        
        return response()->json(['data' => $newData,"status"=>200]);
    }

   
}
