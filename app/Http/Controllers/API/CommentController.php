<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Helper;
use Validator;
use App\Models\Comment;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Helper::getDataBlogComment();
        return response()->json(['data' => $data,"status"=>200]);    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $validator = Validator::make($request->all(), [ 
                
                'description' => 'required', 
                'blog_id' => 'required|alpha_dash|exists:blogs,id', 
                
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors(),"status"=>401], 401);
            }
            $data=$request->all();
            $data['user_id']=Helper::getId();
            $data=Comment::create($data);
            
            return response()->json(['success'=>"Comment Successfully created","status"=>200], 200);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
                'comment_id' => 'required|exists:comments,id',
               
                'description' => 'required', 
                
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors(),"status"=>401], 401);
            }
            $data=Comment::find($request->comment_id);
            if($data){
                if(Helper::getValidActionByCommentID($data)){
                    $data->update($request->all());
                    return response()->json(['success' => "Successfully Edited","status"=>200]);
                }else{
                    return response()->json(['error'=>'Unauthorised',"status"=>401], 401); 
                }
            }else{
                return response()->json(['error'=>'Invalid Blog id',"status"=>404], 404); 
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
                'comment_id' => 'required|exists:comments,id', 
            ]);
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors(),"status"=>401], 401);
            }
            $data=Comment::find($request->comment_id);
            if($data){
                if(Helper::getValidActionByCommentID($data)){
                    $data->delete();
                    return response()->json(['success' => "Successfully deleted","status"=>200]);
                }else{
                    return response()->json(['error'=>'Unauthorised',"status"=>401], 401); 
                }
            }else{
                return response()->json(['error'=>'Invalid Blog id',"status"=>404], 404); 
            }
    }
}
