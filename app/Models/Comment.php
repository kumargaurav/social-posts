<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=[
    	"description","user_id","blog_id"
    ];


    public function user(){
        return $this->belongsTo(\App\User::class,"user_id","id");
    }

    public function blog(){
        return $this->belongsTo(\App\Models\Blog::class,"blog_id","id");
    }

    
    public function getUsernameAttribute()
    {
        return ucfirst($this->user->name);
    }

    public function getEmailAttribute()
    {
        return ucfirst($this->user->email);
    }


}
