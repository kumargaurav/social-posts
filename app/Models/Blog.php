<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable=[
    	 	"name",
            "description",
            "seo_url",
            "title",
            "meta_keywords",
            "meta_description",
            "user_id",
            "longitude",
            "latitude",
            "status",
            'location'
    ];


    public function user(){
        return $this->belongsTo(\App\User::class,"user_id","id");
    }

    public function getUsernameAttribute()
    {
        return ucfirst($this->user->name);
    }

    public function getEmailAttribute()
    {
        return ucfirst($this->user->email);
    }


    public function getCommentcountAttribute()
    {
        return ucfirst($this->comments->count());
    }



    public function comments(){
        return $this->hasMany(\App\Models\Comment::class);
    }
}
