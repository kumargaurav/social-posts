<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(["namespace"=>"API"],function(){


	Route::post('login', 'UserController@login');
	Route::post('register', 'UserController@register');
	Route::group(['middleware' => 'auth:api'], function(){
		Route::post('details', 'UserController@details');
		Route::post('blog-list','BlogController@index');
		Route::post('blog-create','BlogController@store');
		Route::post('blog-show','BlogController@show');
		Route::post('blog-delete','BlogController@destroy');
		Route::post('get-trending-blog','BlogController@getTrendingBlog');
		Route::post('blog-edit','BlogController@update');


		Route::post('comment-list','CommentController@index');
		Route::post('comment-create','CommentController@store');
		Route::post('comment-delete','CommentController@destroy');
		Route::post('comment-edit','CommentController@update');

		Route::post('search','BlogController@search');
		Route::post('logout','UserController@logoutApi');
		
	});

});
Route::fallback(function(){
    return response()->json(['message' => 'Not Found.'], 404);
});