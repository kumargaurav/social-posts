-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('publish','unpublish') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blogs_seo_url_unique` (`seo_url`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `blogs` (`id`, `name`, `description`, `seo_url`, `title`, `meta_keywords`, `meta_description`, `user_id`, `longitude`, `latitude`, `status`, `created_at`, `updated_at`, `location`) VALUES
(1,	'This is my first blog',	'This is my first blog description',	'first-blog123',	'this is title tag',	NULL,	NULL,	'1',	'77.0266',	'28.4595',	'publish',	'2019-08-01 02:01:04',	'2019-08-01 03:15:49',	'Gurgoan'),
(2,	'This is my first blog',	'This is my first blog description',	'first-blog',	'this is title tag',	NULL,	NULL,	'1',	'85.1376',	'25.5941',	'publish',	'2019-08-01 02:02:23',	'2019-08-01 02:02:23',	'patna'),
(4,	'This is my first blog',	'This is my first blog description',	'first-blog1',	'this is title tag',	NULL,	NULL,	'2',	'77.0266',	'28.4595',	'publish',	'2019-08-01 02:32:00',	'2019-08-01 02:32:00',	'Gurgoan'),
(8,	'This is edit eight name',	'This is edit eight description',	'edit-eight-slug',	'This is edit eight title',	'This is edit eight meta keywords',	'This is edit eight description',	'2',	'77.3116',	'28.5850',	'publish',	'2019-08-01 02:01:04',	'2019-08-01 03:07:42',	'noida'),
(9,	'This is my first blog',	'This is my first blog description',	'third-slug',	'this is title tag',	NULL,	NULL,	'1',	'77.3812',	'28.6210',	'publish',	'2019-08-01 02:01:04',	'2019-08-01 02:35:52',	'noida'),
(10,	'This is my first blog',	'This is my first blog description',	'fourth-slug',	'this is title tag',	NULL,	NULL,	'2',	'77.3910',	'28.5355',	'publish',	'2019-08-01 02:01:04',	'2019-08-01 02:35:52',	'noida'),
(11,	'This is my first blog',	'This is my first blog description',	'fifth-slug',	'this is title tag',	NULL,	NULL,	'1',	'77.3910',	'28.5355',	'publish',	'2019-08-01 02:02:23',	'2019-08-01 02:02:23',	'noida'),
(12,	'This is my first blog',	'This is my first blog description',	'first-blog12',	'this is title tag',	NULL,	NULL,	'2',	'77.3812',	'28.6210',	'publish',	'2019-08-01 12:07:44',	'2019-08-01 12:07:44',	'noida'),
(13,	'This is my first blog',	'This is my first blog description',	'first-blog1234',	'this is title tag',	NULL,	NULL,	'2',	'77.3812',	'28.6210',	'publish',	'2019-08-01 12:08:07',	'2019-08-01 12:08:07',	'sector 63,noida');

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `blog_id` (`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `comments` (`id`, `description`, `user_id`, `blog_id`, `created_at`, `updated_at`) VALUES
(1,	'First Comment',	'1',	'1',	NULL,	NULL),
(3,	'From post man comment',	'2',	'2',	'2019-08-01 04:11:28',	'2019-08-01 04:11:28'),
(4,	'From post man comment',	'2',	'4',	'2019-08-01 04:11:40',	'2019-08-01 04:11:40'),
(5,	'This is editable comment by postman',	'2',	'4',	'2019-08-01 04:26:10',	'2019-08-01 04:28:14'),
(6,	'From post man comment',	'2',	'4',	'2019-08-01 04:26:19',	'2019-08-01 04:26:19');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2016_06_01_000001_create_oauth_auth_codes_table',	1),
(4,	'2016_06_01_000002_create_oauth_access_tokens_table',	1),
(5,	'2016_06_01_000003_create_oauth_refresh_tokens_table',	1),
(6,	'2016_06_01_000004_create_oauth_clients_table',	1),
(7,	'2016_06_01_000005_create_oauth_personal_access_clients_table',	1),
(9,	'2019_08_01_065759_create_blogs_table',	2),
(10,	'2019_08_01_083343_create_comments_table',	3),
(11,	'2019_08_01_102857_create_trends_table',	4);

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('5c19627684d0b14bae283714d8ff7a2604a9f58edd4fe7d980e7f0b693090277bed7dd3bfaad6c48',	1,	1,	'MyApp',	'[]',	0,	'2019-08-01 01:14:12',	'2019-08-01 01:14:12',	'2020-08-01 06:44:12'),
('627d070488e7c052f2da6a7abbbf2c7cafe09ef08e7fc3d5ee7322d0d692280dd3eccc7b2f308734',	2,	1,	'MyApp',	'[]',	0,	'2019-08-01 02:11:03',	'2019-08-01 02:11:03',	'2020-08-01 07:41:03'),
('a40e7e8124fe0d215fc3a647be511761a03e1222c8b8b557a7bacc4b4538438ebf126127005d2d21',	1,	1,	'MyApp',	'[]',	0,	'2019-08-01 01:15:21',	'2019-08-01 01:15:21',	'2020-08-01 06:45:21'),
('ccc5ac5bbdb7304c2412b4a41f060eda391e0b2917fe05a20bc63e477d9a945d966a9f62353e49d8',	1,	1,	'MyApp',	'[]',	0,	'2019-08-01 02:31:40',	'2019-08-01 02:31:40',	'2020-08-01 08:01:40');

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1,	NULL,	'Laravel Personal Access Client',	'L1sHCJP3byXvL6fIjFFXQRfaTBujx5Xpt0QMX28W',	'http://localhost',	1,	0,	0,	'2019-08-01 01:03:22',	'2019-08-01 01:03:22'),
(2,	NULL,	'Laravel Password Grant Client',	'T64l6nJjgFhfZeHa9j0NcvuZ40J6HdRWwUzMraBZ',	'http://localhost',	0,	1,	0,	'2019-08-01 01:03:22',	'2019-08-01 01:03:22');

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1,	1,	'2019-08-01 01:03:22',	'2019-08-01 01:03:22');

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `trends`;
CREATE TABLE `trends` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `blog_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `trends` (`id`, `user_id`, `blog_id`, `created_at`, `updated_at`) VALUES
(10,	'2',	'11',	'2019-08-01 03:51:55',	'2019-08-01 10:51:55'),
(11,	'1',	'11',	'2019-07-04 10:52:12',	'2019-08-01 10:52:12'),
(12,	'2',	'1',	'2019-08-01 10:52:15',	'2019-08-01 10:52:15'),
(13,	'1',	'9',	'2019-08-01 10:52:25',	'2019-08-01 10:52:25'),
(14,	'2',	'2',	'2019-08-01 10:52:29',	'2019-08-01 10:52:29'),
(15,	'1',	'8',	'2019-08-01 10:52:36',	'2019-08-01 10:52:36'),
(16,	'2',	'11',	'2019-08-01 10:51:55',	'2019-08-01 10:51:55'),
(17,	'2',	'1',	'2019-08-01 10:52:15',	'2019-08-01 10:52:15');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` enum('admin','user') COLLATE utf8mb4_unicode_ci DEFAULT 'user',
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `type`, `mobile`, `longitude`, `latitude`, `city`, `state`, `country`) VALUES
(1,	'Kumar Gaurav',	'gaurav@mailinator.com',	NULL,	'$2y$10$jpyrtliy4WmiZwS6osPzOeCMUInfeTfE8r3UQnaUwFezTzGhBebK.',	NULL,	'2019-08-01 01:14:12',	'2019-08-01 01:14:12',	'admin',	'9045642302',	'77.3910',	'28.5355',	'noida',	'uttar pardesh',	'India'),
(2,	'Suraj Kumar',	'gaurav1@mailinator.com',	NULL,	'$2y$10$jpyrtliy4WmiZwS6osPzOeCMUInfeTfE8r3UQnaUwFezTzGhBebK.',	NULL,	'2019-08-01 01:14:12',	'2019-08-01 01:14:12',	'user',	'9045642302',	'77.3910° E',	'28.5355° N',	'noida',	'uttar pardesh',	'India');

-- 2019-08-01 12:25:41
